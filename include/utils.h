#ifndef __UTILS_H
#define __UTILS_H

#include <stddef.h>

#define assert(x) if (!(x)) { kputs("Assertion failed: " #x "\n"); for(;;); }

void *memset (void * ptr, int value, size_t num);
void *memcpy(void *dest, const void *src, size_t n);

#endif // __UTILS_H

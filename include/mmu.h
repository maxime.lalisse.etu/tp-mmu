#ifndef __MMU_H__
#define __MMU_H__

#include "idt.h"

#define MAX_PAGES 1024
#define PAGE_SIZE 4096
#define MAX_PROCESSES 4096

typedef struct page_directory_s {
  unsigned int present          : 1;
  unsigned int allow_write      : 1;
  unsigned int uk               : 1;
  unsigned int cache_write_mode : 1;
  unsigned int cache_desactived : 1;
  unsigned int has_been_read    : 1;
  unsigned int                  : 1;
  unsigned int size             : 1;
  unsigned int                  : 4;
  unsigned int page_table_addr  : 20;
} Page_Directory_s;

typedef struct page_entry_s {
  unsigned int present          : 1;
  unsigned int allow_write      : 1;
  unsigned int uk               : 1;
  unsigned int cache_write_mode : 1;
  unsigned int cache_desactived : 1;
  unsigned int has_been_read    : 1;
  unsigned int dirty            : 1;
  unsigned int cache_setting    : 1;
  unsigned int local            : 1;
  unsigned int                  : 3;
  unsigned int page_addr        : 20;
} Page_Entry_s;

void page_fault_handler(int_regs_t *r);

void setup_mmu();

#endif // __MMU_H__

Ce fichier est le rendu de Maxime Lalisse
==========================================================

Définir des structures de données MMU
-------------------------------------




Définir un plan d'adressage
---------------------------

## Q5 Adresse de compilation

Dans le link script (`link.ld`), le code compilé (.text) est placé à l'addresse `phys`, c'est à dire 0x00100000

L'addresse des autres sections (.data, .bss, .stack) n'est pas renseignée et est donc librement choisie par le linker

## Q6 Mémoire virtuelle et noyau

Le code de `my-kernel` est chargé en mémoire à l'adresse 0x00100000.
Cela correspond à idx1 = 0, idx2 = 0b0100000000 = 256 (ou 2 selon le sens de lecture), offset = 0

L'addresse de la mémoire vidéo utilisée par la fonction putc is 0xB8000
Cela correspond à idx1 = 0, idx2 = 184 (ou 0x2e0 selon le sens de lecture), offset = 0

## Q7 Espace mémoire

Il y aurai donc un unique page directory.
Celle ci peut définir 2^10 page entries.
Chaque page entry fait 2^12 octets.
Cela fait donc une taille de 2^10 * 2^12, soit 2^22 octets, soit 4 Mo.

## Q8 Plan d'adressage

On mettra simplement toutes les données du kernel sur 4 Mo à partir de l'adresse 0.

On configure le premier Directory_Entry_s pour pointer sur les 1024 premières pages. Et ces pages pointeront respectivement sur les 4 premiers Mo de la mémoire (la première page pointe sur les 4 premiers Ko, etc...)

Un premier programme _user_
---------------------------

## Q12 Plan d'adressage _user_

Pour notre plan d'adressage user, nous utilisons idx1 = 1,
et idx2 va de 0 à 1024. L'offset va de 0 à 4096.

Cela fait une première adresse à 0b 0000000001 0000000000 000000000000
Et une dernière adresse à        0b 0000000001 1111111111 111111111111

## Question Premier programme _user_ (Q16)

On observe un page fault à l'adresse de `puts`.
C'est cohérent car le code ce `puts` est dans des pages
configurés pour le kernel uniquement

## Q22 Interposition d'appel

Cela protège le noyau des comportements malveillants, car
seul les fonctions exposés via l'interruption 87 sont maintenant accessibles.
Tout le reste du noyau n'est pas accessible depuis l'userspace.

## Q29 Analyse

## Q34 Pile d'appel virtuelle

idx1 doit être 0x7f. (0x40 /*start of userspace*/ + 0x40 - 1)
idx2 peut être n'importe quoi, sa valeur maximale est 0x3ff (0b111111111111)


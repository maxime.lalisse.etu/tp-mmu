#include <stddef.h>

void *memset (void * ptr, int value, size_t num) {
  char * cptr = (char*) ptr;
  for (int i = 0; i < num; i++) {
    cptr[i] = value;
  }
  return ptr;
}

void *memcpy(void *dest, const void *src, size_t n) {
  char *csrc = (char *)src;
  char *cdest = (char *)dest;
  for (int i = 0; i < n; i++) {
    cdest[i] = csrc[i];
  }
  return dest;
}


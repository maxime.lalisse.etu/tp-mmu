#include <stdio.h>
#include <string.h>

#include "mmu.h"
#include "user.h"
#include "utils.h"

extern char _begin_of_user_space;
extern char _end_of_user_space;

extern void kputs(char *aString);
extern void kputhex(unsigned aNumber);
void kclear_screen();

int page_status[MAX_PAGES];
int first_free_page;
int first_free_page_directory;

void *page_to_addr(int page) {
  return (void *)(page * PAGE_SIZE);
}

int addr_to_page(void *addr) {
  return ((int)addr / PAGE_SIZE);
}

void enable_mmu() {
  volatile unsigned int cr0;
  asm("mov %%cr0, %0" : "=r"(cr0));
  cr0 = cr0 | (1 << 31) | (1 << 16);
  asm("mov %0, %%cr0" : : "r"(cr0));
}

void disable_mmu() {
  volatile unsigned int cr0;
  asm("mov %%cr0, %0" : "=r"(cr0));
  cr0 = cr0 & ~((1 << 31) | (1 << 16));
  asm("mov %0, %%cr0" : : "r"(cr0));
}


Page_Entry_s page_table[MAX_PAGES]          __attribute__((aligned (PAGE_SIZE)));
Page_Entry_s user_page_table[MAX_PAGES]     __attribute__((aligned (PAGE_SIZE)));

Page_Directory_s page_directories[MAX_PROCESSES][2] __attribute__((aligned (PAGE_SIZE)));

void *get_cr2(void) {
  volatile unsigned int cr2;
  asm("mov %%cr2, %0" : "=r"(cr2));
  return (void *)cr2;
}

int alloc_page(int page_addr) {
  if (first_free_page <= 0 || page_status[first_free_page] == -1) {
    kputs("No free page left.\n");
    return 0;
  }

  assert(page_addr > 0x400 && page_addr <= 0x800);

  user_page_table[first_free_page].present = 1;
  int next_free_page = page_status[first_free_page];
  page_status[first_free_page] = -1;
  first_free_page = next_free_page;

  kputs("Creating page ");
  kputhex((int) first_free_page);
  kputs("(page_addr  ");
  kputhex((int) page_addr);
  kputs(")");
  kputs("\n");

  for (int i = 0; i < 100000; i++);

  return first_free_page;
}

void free_page(void *page_addr) {
  int page = addr_to_page(page_addr);
  page_status[page] = first_free_page;
  first_free_page = page;
}

void page_fault_handler(int_regs_t *r) {
  void * cr2 = get_cr2();
  unsigned int page_fault = addr_to_page(cr2);
  //unsigned int stack_page = addr_to_page(&user_stack);
  unsigned int guard_page = 0x401;

  if (page_fault > 0x401 && page_fault <= 0x800) {
    if (alloc_page(page_fault)) {
      kputs("Page fault trying to access address ");
      kputhex((int) cr2);
      kputs("(page  ");
      kputhex((int) page_fault);
      kputs(")");
      kputs("\n");
      kputs("New page allocated\n");
    } else {
      kputs("No free page left.\n");
      for(;;);
    }
  } else {
    kputhex((int)page_fault);
    kputs("\n");
    kputhex((int)guard_page);
    kputs("\n");

    if (page_fault == guard_page) {
      kputs("Stack overflow.\n");
    } else {
      kputs("Page fault.\n");
    }

    kputs("Address (");
    kputhex((int)get_cr2());
    kputs(") called by instruction (");
    kputhex(r->eip);
    kputs(")\n");

    for(;;);
  }
}

void setup_mmu() {
  // -- Page status
  first_free_page = MAX_PAGES - 1;
  for (int i = MAX_PAGES - 1; i >= 0; i--) {
    page_status[i] = i - 1;
  }

  first_free_page_directory = 1;

  // -- Page directory
  memset(page_directories[0], 0, MAX_PAGES * sizeof(Page_Directory_s));
  for (int i = 0; i < MAX_PAGES; i++) {
    page_directories[0][i].allow_write      = 1;
    page_directories[0][i].uk               = 1;
    page_directories[0][i].size             = 1;
  }

  // -- Page table (kernel)
  memset(page_table, 0, MAX_PAGES * sizeof(Page_Entry_s));
  for (int i = 0; i < MAX_PAGES; i++) {
    page_table[i].present          = 1;
    page_table[i].allow_write      = 1;
    page_table[i].uk               = 0;
    page_table[i].page_addr        = i;
  }

  // -- Page table (user)
  memset(user_page_table, 0, MAX_PAGES * sizeof(Page_Entry_s));
  for (int i = 0; i < MAX_PAGES; i++) {
    user_page_table[i].present          = 0;
    user_page_table[i].allow_write      = 1;
    user_page_table[i].uk               = 1;
    user_page_table[i].page_addr        = i + 1024;
  }
  // Use code (.text section) as read-only
  user_page_table[4].present = 1;
  user_page_table[4].allow_write = 0;

  // Kernel stack
  for (int i = 0; i < 4; i++) {
    user_page_table[i].present = 1;
    user_page_table[i].uk = 1;
  }

  page_directories[0][0].present = 1;
  page_directories[0][0].uk      = 0;
  page_directories[0][0].page_table_addr = ((unsigned int)page_table) >> 12;

  page_directories[0][1].present = 1;
  page_directories[0][1].uk      = 1;
  page_directories[0][1].page_table_addr = ((unsigned int)user_page_table) >> 12;

  /*
  // -- Debug
  unsigned int user_start_page = addr_to_page(&_begin_of_user_space);
  unsigned int user_end_page   = addr_to_page(&_end_of_user_space);
  unsigned int num_user_page   = (user_end_page - user_start_page);

  kputs("Start page: ");
  kputhex((int)user_start_page);
  kputs("\n");
  kputs("End page: ");
  kputhex((int)user_end_page);
  kputs("\n");
  */

  //user_page_table[1].present = 0;
  
  asm("mov %0, %%cr3" : : "r"(page_directories[0]));
  enable_mmu();
}

void page_copy(char *pg_src, char *pg_dest) {
  disable_mmu();
  memcpy(pg_dest, pg_src, PAGE_SIZE);
  enable_mmu();
}

Page_Directory_s *clone_page_directory(Page_Directory_s *src_pd) {
  memcpy(page_directories[first_free_page_directory], src_pd, PAGE_SIZE);
  return page_directories[first_free_page_directory++];
}

#include "user.h"

//char user_stack[USER_STACK_SIZE] __attribute__((aligned (4096)))
//  = {0};

void syscall(int n, void *arg) {
  asm("mov %0, %%eax" : : "r"(n) : "eax");
  asm("mov %0, %%edx" : : "r"(arg) : "eax");
  asm("int $87");
}

void puts(char *aString) {
  syscall(0, aString);
}

void putc(char c) {
  int cccc = c;
  syscall(1, (void*)cccc);
}

//void puthex(char *aString) {
//  syscall(2, aString);
//}

void clear_screen() {
  syscall(3, 0);
}

int fact(int n) {
  if (n <= 0) {
    return 1;
  } else {
    return n * fact(n-1);
  }
}

void user_main() {
  puts("Hello world from user land\n");
  fact(2007);
  puts("Hello after dynamic stack allocation\n");
  for (;;);
}

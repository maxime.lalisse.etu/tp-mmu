#include "ioport.h"
#include "mmu.h"
#include "gdt.h"
#include "idt.h"
#include "user.h"
#include "call_user.h"

extern char _begin_of_user_space;
extern char _end_of_user_space;

void kclear_screen();				/* clear screen */
void kputc(char aChar);				/* print a single char on screen */
void kputs(char *aString);			/* print a string on the screen */
void kputhex(unsigned aNumber);			/* print an Hex number on screen */

void empty_irq(int_regs_t *r) {}

void my_syscall_handler(int_regs_t *r) {
  switch (r->eax) {
    case 0:
      kputs((char*)r->edx);
      break;
    case 1:
      kputc((char)r->edx);
      break;
    case 2:
      kputhex((char)r->edx);
      break;
    case 3:
      kclear_screen();
      break;
    default:
      kputs("Unknown syscall.\n");
  }
}

/* multiboot entry-point with datastructure as arg. */
void main(unsigned int * mboot_info)
{
    /* clear the screen */
    kclear_screen();
    kputs("Early boot.\n"); 
    kputs("\t-> Setting up the GDT... ");
    gdt_init_default();
    kputs("OK\n");

    kputs("\t-> Setting up the IDT... ");
    setup_idt();
    kputs("OK\n");

    kputs("\n\n");

    idt_setup_irq_handler(0, empty_irq);
    idt_setup_irq_handler(1, empty_irq);

    __asm volatile("sti");

    /* minimal setup done ! */

    /* setup page fault handler */
    idt_setup_int_handler(14, page_fault_handler);
    idt_setup_int_handler(87, my_syscall_handler);

    kputs("== Setup MMU ==\n");
    setup_mmu();

    kputs("== Begin userspace ==\n");

    kputhex(((int)&_begin_of_user_space));
    kputs("\n");
    kputhex(((int)&_end_of_user_space));
    kputs("\n");

    //void * start_of_stack = user_stack + USER_STACK_SIZE - 4;
    void * start_of_stack = (void*)0x00400000 + MAX_PAGES * 4096 - 4;
    kputhex(((int)start_of_stack));
    kputs("\n");
    call_user(user_main, start_of_stack);

    kputs("Going idle!\n");

    for(;;) ; /* nothing more to do... really nothing ! */
}

/* base address for the video output assume to be set as character oriented by the multiboot */
unsigned char *video_memory = (unsigned char *) 0xB8000;

/* clear screen */
void kclear_screen() {
  int i;
  for(i=0;i<80*25;i++) { 			/* for each one of the 80 char by 25 lines */
    video_memory[i*2+1]=0x0F;			/* color is set to black background and white char */
    video_memory[i*2]=(unsigned char)' '; 	/* character shown is the space char */
  }
}

/* print a string on the screen */
void kputs(char *aString) {
  char *current_char=aString;
  while(*current_char!=0) {
    kputc(*current_char++);
  }
}

/* print an number in hexa */
char *hex_digit="0123456789ABCDEF";
void kputhex(unsigned aNumber) {
  int i;
  int started=0;
  for(i=28;i>=0;i-=4) {
    int k=(aNumber>>i)&0xF;
    if(k!=0 || started) {
      kputc(hex_digit[k]);
      started=1;
    }
  }
  if(!started) kputc('0');
}

/* print a char on the screen */
int cursor_x=0;					/* here is the cursor position on X [0..79] */
int cursor_y=0;					/* here is the cursor position on Y [0..24] */

void setCursor() {
  int cursor_offset = cursor_x+cursor_y*80;
  _outb(0x3d4,14);
  _outb(0x3d5,((cursor_offset>>8)&0xFF));
  _outb(0x3d4,15);
  _outb(0x3d5,(cursor_offset&0xFF));
}

void kputc(char c) {
  if(cursor_x>79) {
    cursor_x=0;
    cursor_y++;
  }
  if(cursor_y>24) {
    cursor_y=0;
    kclear_screen();
  }
  switch(c) {					/* deal with a special char */
    case '\r': cursor_x=0; break;		/* carriage return */
    case '\n': cursor_x=0; cursor_y++; break; 	/* new ligne */	
    case 0x8 : if(cursor_x>0) cursor_x--; break;/* backspace */
    case 0x9 : cursor_x=(cursor_x+8)&~7; break;	/* tabulation */
						/* or print a simple character */
    default  : 
      video_memory[(cursor_x+80*cursor_y)*2]=c;
      cursor_x++;
      break;
  }
  setCursor();
}
